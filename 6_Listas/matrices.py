# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 3 de abril, 2017.
# Ejemplos de operaciones con matrices (listas de
# listas).
#----------------------------------------------------------

def crea_matriz(ren, col, inicial):
    r = []
    for i in range(ren):
        r.append([inicial] * col)
    return r

def suma_matriz_escalar(a, k):
    ren = len(a)
    col = len(a[0])
    r = crea_matriz(ren, col, 0)
    for i in range(ren):
        for j in range(col):
            r[i][j] = a[i][j] + k
    return r

def suma_matrices(a, b):
    if len(a) != len(b) or len(a[0]) != len(b[0]):
        raise ValueError('Matrices deben ser del mismo tamaño')
    ren = len(a)
    col = len(a[0])
    r = crea_matriz(ren, col, 0)
    for i in range(ren):
        for j in range(col):
            r[i][j] = a[i][j] + b[i][j]
    return r

def traspuesta(m):
    ren = len(m)
    col = len(m[0])
    r = crea_matriz(col, ren, 0)
    for i in range(ren):
        for j in range(col):
            r[j][i] = m[i][j]
    return r

def main():
    m1 = [[1, 5, -1],
          [4, 0, 8]]
    m2 = [[2, 1, 3],
          [3, -1, 1]]
    m3 = suma_matriz_escalar(m1, 10)
    m4 = suma_matrices(m1, m2)
    m5 = traspuesta(m1)
    print(m3)
    print()
    print(m4)
    print()
    print(m5)

main()