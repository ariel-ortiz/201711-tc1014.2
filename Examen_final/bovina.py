# -*- coding: utf-8 -*-
# Solución al problema 1 del examen final práctico.

def bovina(a, b):
    suma = 0
    for i in str(a):
        for j in str(b):
             suma += int(i) * int(j)
    return suma

def main():
    print(bovina(123, 45))
    print(bovina(1138, 2187))
    print(bovina(123456789999999, 11123432111))
    print(bovina(1, 1984) + bovina(200, 13321))

main()