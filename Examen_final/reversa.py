# -*- coding: utf-8 -*-
# Solución al problema 2 del examen final práctico.

def reversa(n, cadena):
    total = len(cadena)
    paso = total // n
    resultado = ''
    for i in range(0, total, paso):
        resultado += cadena[i:i + paso][::-1]
    return resultado

def main():
    print(reversa(5, 'VOYASACARUNCIEN'))
    print(reversa(4, 'creo_que_soy_demasiado_optimista'))
    print(reversa(6, 'tantan'))
    print(reversa(1, 'tantan'))

main()