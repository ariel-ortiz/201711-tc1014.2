# -*- coding: utf-8 -*-
# Solución al problema 4 del examen final práctico.

def convierte(n):
    if n == 0:
        return ['@']
    resultado = []
    while n > 0:
        residuo = n % 20
        cincos = residuo // 5
        unidades = residuo % 5
        if residuo == 0:
            resultado.append('@')
        else:
            resultado.append(unidades * '.' + cincos * '-')
        n //= 20
    return resultado[::-1]

def maya(salida, nums):
    with open(salida, 'w') as archivo:
        for x in nums:
            print('===', x, '===', file=archivo)
            r = convierte(x)
            for d in r:
                print(d, file=archivo)
            print(file=archivo)
        print('=== fin ===', file=archivo)

def valor(renglon):
    resultado = 0
    for c in renglon:
        if c == '.':
            resultado += 1
        elif c == '-':
            resultado += 5
    return resultado

def decimal(cadena_maya):
    resultado = 0
    for renglon in cadena_maya.split():
        resultado = (resultado * 20) + valor(renglon)
    return resultado

def main():
    maya('salida1.txt', [32, 429, 5125])
    maya('salida2.txt', [0, 401, 2017, 2 ** 100])
    print(decimal("-\n@\n..---"))
    print(decimal("@"))
    print(decimal("..--\n.---\n-"))
    print(decimal(".\n--\n....\n....-\n-\n...\n-\n.--\n..-\n...--\n..-\n-\n.\n"
        + "..-\n....-\n---\n....---\n-\n....---\n---\n@\n...--\n...-\n.---"))

main()