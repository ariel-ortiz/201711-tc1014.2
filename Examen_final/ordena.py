# -*- coding: utf-8 -*-
# Solución al problema 3 del examen final práctico.

def ordena(entrada, salida):
    with open(entrada, 'r') as arch_entrada:
        numeros = []
        for linea in arch_entrada:
            numeros.append(int(linea))
    numeros.sort()
    with open(salida, 'w') as arch_salida:
        for n in numeros:
            print(n, file=arch_salida)

def main():
    ordena('in1.txt', 'out1.txt')
    ordena('in2.txt', 'out2.txt')

main()