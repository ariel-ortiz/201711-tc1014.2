# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 27 de marzo, 2017.
# Solución al problema 2 del segundo examen práctico.
#----------------------------------------------------------

from math import isclose

def babilonico(n):
    a = 0
    r = 0
    x = n
    while not isclose(a, x):
        a = x
        r = n / x
        x = (r + x) / 2
    return x

def main():
    print(babilonico(2))
    print(babilonico(4))
    print(babilonico(5))
    print(babilonico(9))

main()