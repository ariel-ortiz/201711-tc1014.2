# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 27 de marzo, 2017.
# Solución al problema 1 del segundo examen práctico.
#----------------------------------------------------------

def divisible_entre_7(n):
    if n % 7 == 0:
       return True
    else:
        return False

def contiene_digito_7(n):
#    while n > 0:
#        r = n % 10
#        n //= 10
#        if r == 7:
#            return True
#    return False
    if '7' in str(n):
        return True
    else:
        return False

def sietes(n):
    if divisible_entre_7(n) or contiene_digito_7(n):
        return True
    else:
        return False

def main():
    print(sietes(1064))
    print(sietes(1070))
    print(sietes(1071) and sietes(666666666666))
    print(sietes(6))
    print(sietes(1066))
    print(sietes(1069) or sietes(6666666666666))

main()