# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 27 de marzo, 2017.
# Solución al problema 3 del segundo examen práctico.
#----------------------------------------------------------

def intercambia(palabra):
    if len(palabra) < 2:
        return palabra
    inicio = palabra[0]
    final = palabra[-1]
    medio = palabra[1:-1]
    return final + medio + inicio

def main():
    print(intercambia('portugues'))
    print(intercambia('amigo'))
    print(intercambia('elefante'))
    print(intercambia('ir'))
    print(intercambia('a'))

main()