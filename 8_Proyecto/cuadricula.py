# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 27 de abril, 2017.
# Algunas funciones útiles para el proyecto.
#----------------------------------------------------------

from random import choice

def crea_matriz(ren, col, inicial):
    r = []
    for i in range(ren):
        r.append([inicial] * col)
    return r

def imprime_cuadricula(m):
    raya = '+' + ('-' * 6 + '+') * 4
    print(raya)
    for i in range(4):
        print('|', end='')
        for j in range(4):
            print('{0:5}'.format(m[i][j]), end=' |')
        print()
        print(raya)

def tira_al_azar(m):

    # Guarda las coordenadas de todas las casillas desocupadas.
    disponibles = []
    for i in range(4):
        for j in range(4):
            if m[i][j] == '':
                disponibles.append([i, j])

    # Selecciona al azar una de las casillas desocupadas.
    ren, col = choice(disponibles)
    m[ren][col] = 1

def fin_de_juego(m):
    for i in range(4):
        for j in range(4):

            # ¿Soy una casilla vacía?
            if m[i][j] == '':
                return False

            # ¿Mi vecino de la derecha es igual a mí?
            if j < 3 and m[i][j] == m[i][j + 1]:
                return False

            # ¿Mi vecino de abajo es igual a mí?
            if i < 3 and m[i][j] == m[i + 1][j]:
                return False

    return True

def main():
    m = crea_matriz(4, 4, '')
    m[3][0] = 1024
    tira_al_azar(m)
    imprime_cuadricula(m)
    print()
    print(fin_de_juego(m))

main()