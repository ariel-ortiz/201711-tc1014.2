# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 24 de abril, 2017.
# Ejemplo de como programar un menú.
#----------------------------------------------------------

def menu():
    while True:
        print('''

*** EJEMPLO DE MENU ***

=======================
1. Opción 1
2. Opción 2
3. Opción 3
4. Salir
=======================
        ''')
        opcion = input('Selecciona una opción del 1 al 4: ')
        print()
        if opcion == '1':
            print('Opción 1')
        elif opcion == '2':
            print('Opcion 2')
        elif opcion == '3':
            print('Opcion 3')
        elif opcion == '4':
            break
    print('Bye bye!')


def main():
    menu()

main()