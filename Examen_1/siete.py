# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 13 de febrero, 2017
# Solución al problema 2 del primer examen práctico.
#----------------------------------------------------------

def main():
    for i in range(1, 13):
        print(i * 7, '/', i, '= 7')

main()