# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 13 de febrero, 2017
# Solución al problema 1 del primer examen práctico.
#----------------------------------------------------------

from math import log

def tasa_de_crecimiento(No, Nt, t):
    r = log(Nt / No) * (1 / t) * 100
    return r

def main():
    No = int(input('Población inicial: '))
    Nt = int(input('Población final: '))
    t = int(input('Tiempo en años: '))
    r = tasa_de_crecimiento(No, Nt, t)
    print('La tasa anual de crecimiento es:', r, 'porciento')

main()