# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 13 de febrero, 2017
# Solución al problema 3 del primer examen práctico.
#----------------------------------------------------------

def pi(n):
    m = 1
    for k in range(1, n + 1):
        m *= (4 * k ** 2)/(4 * k ** 2 - 1)
    return m * 2

def main():
    for i in range(0, 6):
        n = 10 ** i
        print(n, pi(n))

main()