# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 6 de marzo, 2017.
# Calcula el máximo común divisor de dos números x, y.
#----------------------------------------------------------

def mcd(x, y):
    resultado = x
    while not (x % resultado == 0 and y % resultado == 0):
        resultado -= 1
    return resultado

def main():
    print(mcd(15, 20))
    print(mcd(20, 13))
    print(mcd(50, 25))
    print(mcd(300, 120))
    print(mcd(1, 45))

main()
