# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 6 de marzo, 2017.
# Solución al problema 1 de la práctica 5.
# Muesta cómo usar un "while True" y break.
#----------------------------------------------------------

def main():
    suma = 0
    c = 0
    while True:
        x = int(input('Input an integer number (0 to quit): '))
        if x == 0:
            break
        suma += x
        c += 1
    print(c, 'numbers were typed.')
    print('The sum of all those numbers is:', suma)
    if c == 0:
        print('Cannot compute average.')
    else:
        print('The average of all those number is:', suma / c)

main()
