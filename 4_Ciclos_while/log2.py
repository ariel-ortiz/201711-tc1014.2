# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 23 de febrero, 2017.
#----------------------------------------------------------

# Devuelve el logaritmo base 2 de n como un número entero.
# En otras palabras, calcula cuántas veces se puede dividir
# n entre 2.
def log2(n):
    contador = -1
    while n > 0:
        contador += 1
        n //= 2
    return contador

def main():
    print(log2(5))
    print(log2(8))
    print(log2(1024))
    print(log2(1023))
    print(log2(0))

main()
