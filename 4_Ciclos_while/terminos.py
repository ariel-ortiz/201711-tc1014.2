# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 6 de marzo, 2017.
# Calcula cuantos términos hay en la serie 1+2+3+...=s
# tal que s es el entero más chico en donde s >= n.
#----------------------------------------------------------

def terminos(n):
    i = 0
    sum = 0
    while sum < n:
        i += 1
        sum += i
    return i

def main():
    print(terminos(15))
    print(terminos(5))
    print(terminos(30))
    print(terminos(5000))

main()