# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 16 de marzo, 2017.
# Dibuja las curvas de dragón (fractal de Parque Jurásico).
#----------------------------------------------------------

from turtle import lt, rt, fd, done, speed, ht, pensize, pencolor

def cambia(s):
    r = ''
    for c in s:
        if c == 'I':
            r += 'D'
        else:
            r += 'I'
    return r

def curva(n):
    c = ''
    for i in range(n):
        reversa = c[::-1]
        complemento = cambia(reversa)
        c = c + 'I' + complemento
    return c

def dragon(n, t):
    fd(t)
    for dob in curva(n):
        if dob == 'I':
            lt(90)
        else:
            rt(90)
        fd(t)

def main():
    speed(0)
    pensize(2)
    pencolor('blue')
    ht()
    lt(90)
    dragon(10, 5)
    done()

main()
