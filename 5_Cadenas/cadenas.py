# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 9 de marzo, 2017.
# Primeros ejemplos de maninpulación de cadenas de
# caracteres.
#----------------------------------------------------------

def convertir_efe(mensaje):
    resultado = ''
    for c in mensaje:
        if c in 'aeiou':
            resultado += (c + 'f' + c)
        else:
            resultado += c
    return resultado

def convertir_a(mensaje):
    resultado = ''
    for c in mensaje:
        if c in 'eiou':
            resultado += 'a'
        else:
            resultado += c
    return resultado

def cuenta_consonantes(mensaje):
    resultado = 0
    for c in mensaje:
        if c in 'bcdfghjklmnpqrstvwxyz':
            resultado += 1
    return resultado

def main():
    print(convertir_efe('hola todos'))
    print(convertir_efe('¿cómo estás?'))
    print(convertir_a('hola todos'))
    print(convertir_a('una mosca parada en la pared'))
    print(cuenta_consonantes('hola todos'))
    print(cuenta_consonantes('una mosca parada en la pared'))

main()