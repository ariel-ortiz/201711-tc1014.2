# -*- coding: utf-8 -*-

# Primer ejemplo definiendo y usando funciones junto
# con gráficas de tortuga.

from turtle import fd, lt, done, goto, pu, pd
from turtle import pencolor, pensize, speed, ht
from turtle import circle, st

def poligono(lados, distancia):
    for i in range(lados):
        fd(distancia)
        lt(360 // lados)

def main():
    ht()
    speed('fast')
    pencolor('navy')
    pensize(5)
    poligono(5, 150) # Dibuja un pentágono.
    pu()
    goto(-200, -100)
    pd()
    pencolor('PeachPuff')
    pensize(10)
    poligono(3, 200) # Dibuja un triángulo.
    pu()
    goto(50, -150)
    pd()
    pencolor('#FF00FF')
    pensize(3)
    poligono(4, 100) # Dibuja un cuadrado.
    pencolor('blue')
    pensize(5)
    st()
    circle(100, 90) # Dibuja un arco.
    lt(180)
    circle(100, 90) # Dibuja otro arco.
    done()

main()