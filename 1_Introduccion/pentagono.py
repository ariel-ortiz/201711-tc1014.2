# -*- coding: utf-8 -*-

# Utiliza gráficas de tortuga para
# dibujar un pentágono.

from turtle import fd, done, lt

for i in range(5):
    fd(100)
    lt(72)

done()

