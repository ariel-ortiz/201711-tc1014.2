# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 23 de enero, 2017.
#
# Pide al usuario el radio de un círculo. Calcula e
# imprime el perímetro y área de dicho círculo.
#----------------------------------------------------------

from math import pi

def perimetro(r):
    resultado = pi * r * 2
    return resultado

def area(r):
    resultado = pi * r ** 2
    return resultado


def main():
    r = float(input('Dame el valor del radio del círculo: '))
    p = perimetro(r)
    a = area(r)
    print('El perímetro del círculo es:', p)
    print('El área del círculo es:', a)

main()
