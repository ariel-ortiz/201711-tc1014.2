# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 26 de enero, 2017.
#
# Calcula el costo total de la renta de videos nuevos y
# viejos. Los videos nuevos se rentan a $3.00, mientras
# que los videos viejos se rentan a $2.00.
#----------------------------------------------------------

def total_cost(new, old):
    cost = new * 3 + old * 2
    return cost

def main():
    n = int(input('Number of new videos to rent: '))
    o = int(input('Number of old videos to rent: '))
    c = total_cost(n, o)
    print('Total charge for costumer\'s video rentals: $', c)

main()