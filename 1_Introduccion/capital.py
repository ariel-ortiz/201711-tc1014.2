# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 26 de enero, 2017.
#
# Calcula un capital final (interés compuesto) a partir de
# un capital inicial C, una tasa de interés i, y un número
# de periodos t.
#----------------------------------------------------------

def capital_final(C, i, t):
    F = C * (1 + i) ** t
    return F

def main():
    w = float(input('Capital inicial: '))
    x = float(input('Tasa de interés: '))
    y = int(input('Periodos: '))
    z = capital_final(w, x, y)
    print('El capital final es', z)

main()