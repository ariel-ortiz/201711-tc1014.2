# -*- coding: utf-8 -*-

#------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 19 de enero, 2017.
# Dibuja la espiral de oro.
#------------------------------

from math import sqrt
from turtle import circle, done, pensize

FI = 2 / (sqrt(5) - 1)

def espiral(n):
    lado = 2
    for i in range(n):
        circle(lado, 90)
        lado = lado * FI

def main():
    pensize(3)
    espiral(11)
    done()

main()
