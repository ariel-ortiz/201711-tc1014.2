# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 23 de enero, 2017.
#
# Pide al usuario como entrada una temperatura en grados
# Fahrenheit (°F) y calcula e imprima su correspondiente
# conversión a grados Celsius (°C) y Kelvin (K).
#
# Utiliza las siguientes fórmulas de conversión:
# °F = 9/5(°C)+32
# °C = K – 273.15
#----------------------------------------------------------

def celsius(f):
    resultado = 5 / 9 * (f - 32)
    return resultado

def kelvin(c):
    resultado = c + 273.15
    return resultado

def main():
    temp = float(input('Grados Fahrenheit: '))
    c = celsius(temp)
    k = kelvin(c)
    print(temp, 'grados Fahrenheit es igual')
    print(c, 'grados Celsius y a', k, 'Kelvin.')

main()
