# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 20 y 23 de febrero, 2017.
#----------------------------------------------------------

# Regresa True si el año y es un año bisiesto, o False en
# caso contrario.
def leap(y):
    if y % 4 == 0:
        if y % 100 == 0:
            if y % 400 == 0:
                return True
            else:
                return False
        else:
            return True
    else:
        return False

# Regresa la fecha del siguiente día después de la fecha
# (y, m, d) enviada como argumento.
def next_day(y, m, d):
    if m == 12 and d == 31:
        return y + 1, 1, 1
    if m in [1, 3, 5, 7, 8, 10] and d == 31:
        return y, m + 1, 1
    if m in [4, 6, 9, 11] and d == 30:
        return y, m + 1, 1
    if m == 2 and leap(y) and d == 29:
        return y, 3, 1
    if m == 2 and not leap(y) and d == 28:
        return y, 3, 1
    return y, m, d + 1

def main():
    print(leap(2016))
    print(leap(2017))
    print(leap(2000))
    print(leap(2100))
    print(leap(2400))
    print(leap(2012))
    print(leap(1900))
    print(next_day(2017, 2, 23))
    print(next_day(2017, 2, 28))
    print(next_day(2016, 2, 28))
    print(next_day(2017, 12, 31))
    print(next_day(2017, 2, 35))  # Error GiGo (Garbage-in Garbage-out)

main()
