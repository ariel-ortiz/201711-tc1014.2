# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 15 de febrero, 2017.
# Calcula el índice de masa corporal.
#----------------------------------------------------------

def bmi(peso, altura):
    indice = peso / altura ** 2
    if indice < 20:
        return "underweigt"
    elif indice < 25:
        return "normal"
    elif indice < 30:
        return "obsese1"
    elif indice < 40:
        return "obsese2"
    else:
        return "obsese3"

def main():
    print(bmi(50, 1.5))
    print(bmi(30, 1.8))
    print(bmi(150, 1.3))

main()
