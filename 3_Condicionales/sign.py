# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 15 de febrero, 2017.
# Función sign(n) regresa -1 si n es negativo, 1 si es
# positivo mayor a cero, o 0 si es igual a cero.
#----------------------------------------------------------

def sign(n):
    if n < 0:
        return -1
    if n > 0:
        return 1
    return 0

def main():
    print(sign(-5))
    print(sign(10))
    print(sign(0))

main()
