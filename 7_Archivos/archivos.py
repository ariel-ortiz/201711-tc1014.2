# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 6 y 17 de abril, 2017.
# Ejemplos de lectura y escritura de archivos.
#----------------------------------------------------------

def main():

    # Lectura usando read()
    with open('datos.txt', 'r') as f:
        s = f.read()
    print(s.upper())

    print()

    # Lectura usando readline()
    with open('datos.txt', 'r') as f:
        s1 = f.readline()
        s2 = f.readline()
    print(s1.strip())
    print(s2.strip())

    print()

    # Lectura usando readlines()
    with open('datos.txt', 'r') as f:
        lst = f.readlines()
    print(lst)
    print()
    for linea in lst:
        print(linea.strip())

    # Escritura usando write()
    with open('salida.txt', 'w') as f:
        f.write('este es un ejemplo\n')
        f.write('de un archivo de salida')

    # Escritura usando print()
    with open('numeros.txt', 'w') as f:
        print('Mis números:', file=f)
        for i in range(1, 11):
            print(i, file=f)

main()