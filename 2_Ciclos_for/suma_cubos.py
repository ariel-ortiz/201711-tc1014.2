# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 2 de febrero, 2017.
# Calcula 1^3 + 2^2 + 3^3 + ... + n^3
#----------------------------------------------------------

def suma_cubos(n):
    a = 0
    for k in range(1, n + 1):
        a += k ** 3
    return a

def main():
    print(suma_cubos(3))
    print(suma_cubos(1000))
    print(suma_cubos(1000000000))

main()