# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 30 de enero, 2017.
# C�lculo del factorial de un n�mero.
#----------------------------------------------------------

def factorial(n):
    r = 1
    for i in range(1, n + 1):
        r *= i
    return r

def main():
    print(factorial(5))
    print(factorial(0))

main()