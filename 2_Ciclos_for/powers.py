# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 2 de febrero, 2017.
# Solución al problema 1 de la práctica #3.
# Imprime los valores de n y 2^n, donde n = 1, 2, ..., 10.
#----------------------------------------------------------

def main():
    for n in range(1, 11):
        print(n, 2 ** n)

main()