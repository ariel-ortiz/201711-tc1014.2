# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 30 de enero, 2017.
# Primer ejemplo usando un ciclo for.
#----------------------------------------------------------

def main():
    s = 0
    for x in range(10, 20, 3):
        s += x
    print(s)

main()