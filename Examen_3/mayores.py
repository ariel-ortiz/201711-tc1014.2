# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 24 de abril, 2017.
# Solución al problema 1 del tercer examen práctico.
#----------------------------------------------------------

def mayores(x):
    if x == []:
        return 0
    promedio = sum(x) / len(x)
    contador = 0
    for e in x:
        if e > promedio:
            contador += 1
    return contador

def main():
    print(mayores([4, 1, 5, 2]))
    print(mayores([-4.2, 0.0, 8.5, 9.1, -100.2, 3.4]))
    print(mayores([7]))
    print(mayores([]))

main()