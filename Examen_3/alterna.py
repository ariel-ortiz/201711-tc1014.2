# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 24 de abril, 2017.
# Solución al problema 3 del tercer examen práctico.
#----------------------------------------------------------

def alterna(entrada, salida):
    with open(entrada, 'r') as f:
        lineas = f.readlines()
    with open(salida, 'w') as f:
        for linea in lineas[::2]:
            print(linea.rstrip(), file=f)

def main():
    alterna('spiderpig.txt', 'salida1.txt')
    alterna('softkitty.txt', 'salida2.txt')

main()