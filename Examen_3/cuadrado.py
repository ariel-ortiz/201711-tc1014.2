# -*- coding: utf-8 -*-

#----------------------------------------------------------
# Autor: Ariel Ortiz R.
# Fecha: 24 de abril, 2017.
# Solución al problema 2 del tercer examen práctico.
#----------------------------------------------------------

def cuadrado(x):
    resultado = []
    for e in x:
        resultado.append(e ** 2)
    return resultado

def main():
    print(cuadrado([2, 5, 10, 12]))
    print(cuadrado([-4, -3, -2, -1, 0]))
    print(cuadrado([]))

main()